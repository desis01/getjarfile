FROM openjdk:8-jre-alpine

ENV APP_INTERNAL_PORT=8086

EXPOSE ${APP_INTERNAL_PORT}

ADD target/*.jar app.jar

CMD ["java","-jar","app.jar"]
