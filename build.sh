action=$1
name=${2:=testservice}
version=${3:=latest}
registry=${4:=registry.gitlab.com}
username=${5:=ahahah}
password=${6:=hahaha}

if [ $version = 'auto-version' ]
then
    version=`cat src/main/resources/VERSION`
    echo "version: $version"
fi

imageName=$name:latest
registryImageNameLatest=$registry/$name:latest
registryImageNameVersion=$registry/$name:$version

# docker run --name my registry.gitlab.com/desis01/jarfileclass:1.0.1e

# docker cp getjarfile:app.jar .


# mvn install:install-file -Dfile=/target/jarfileclass-0.1.2-SNAPSHOT.jar -DgroupId=com -DartifactId=jarfileclass -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar

# mvn dependency:get -Dartifact=com::jarfileclass-0.1.1-SNAPSHOT -DremoteRepositories=gitlab-maven::::https://gitlab.com/api/v4/projects/45097979/packages  -s settings.xml
echo "Maven clean starts"
mvn clean

mkdir target

echo "Copy jar file"
scp /home/gitlab-runner/builds/PSMB-fsd/0/desis01/jarfileclass/target/jarfileclass-0.1.0-SNAPSHOT.jar /home/gitlab-runner/builds/PSMB-fsd/0/desis01/getjarfile/target/

echo "Maven package starts"
 mvn package

# cd /lib
# ls -al

# docker build -f Dockerfile . -t $imageName

# docker tag $imageName $registryImageNameLatest
# docker tag $imageName $registryImageNameVersion

# echo "Login into GitLab Registry."; docker login -u=$username -p=$password https://registry.gitlab.com/v2/

# docker push $registryImageNameLatest
# docker push $registryImageNameVersion

# docker logout https://registry.gitlab.com/v2/
